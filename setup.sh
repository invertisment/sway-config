#!/bin/bash

# Run this script from ~/.config/sway
DIR="~/.config/sway"

echo "# Sway specific config" >> ~/.bashrc
echo "source $DIR/bashrc/bashrc" >> ~/.bashrc
echo "" >> ~/.bashrc

mkdir -p ~/.config/gtk-3.0
ln -s $DIR/gtk-3.0/settings.ini ~/.config/gtk-3.0/settings.ini || echo -n

mkdir -p ~/.config/alacritty
ln -s $DIR/alacritty/alacritty.yml ~/.config/alacritty/alacritty.yml || echo -n
