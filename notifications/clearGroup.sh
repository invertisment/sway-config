#!/bin/bash

GROUP=$1

makoctl list | \
  jq '.data | .[] | .[] | {mm_category: .category|.data, mm_id: .id|.data} | select(.mm_category == $find_cat) | .mm_id' \
  --arg find_cat $GROUP | \
  xargs -L 1 makoctl dismiss -n

